package com.be.kdk.testbuttonjack;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import org.json.JSONException;
import org.json.JSONObject;

public class SendCouloirActivity extends AppCompatActivity  implements SendCouloirInterface {

    EditText etIdCouloir;
    Button btnSendIdCouloir;
    TextView tvErrorMessage;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_send_couloir);
        etIdCouloir = findViewById(R.id.etIdCouloir);
        btnSendIdCouloir = findViewById(R.id.btnSendRequest);
        tvErrorMessage = findViewById(R.id.tvErrorMessage);
    }

    public void sendIdCouloir(View view) {
        tvErrorMessage.setVisibility(View.INVISIBLE);
        String couloirString = etIdCouloir.getText().toString();
        try{
            int couloir = Integer.parseInt(couloirString);
            if(couloir >=0 && couloir < 20){
                HttpRequestsClass httpRequestsClass = new HttpRequestsClass();
                httpRequestsClass.sendCouloirBooking(this, couloir);
            }
            else{
                setErrorMessageText();
            }
        }
        catch(NumberFormatException | NullPointerException ex)
        {
            setErrorMessageText();
        }
    }

    private void setErrorMessageText()
    {
        tvErrorMessage.setText("Entrez un numéro de couloir valide");
        tvErrorMessage.setVisibility(View.VISIBLE);
    }

    @Override
    public void sendPositiveResponseToService(String response) {
        this.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                try {
                    Intent intent = new Intent(getApplicationContext(),MainActivity.class);
                    JSONObject jsonObject = new JSONObject(response);
                    String idSeries = jsonObject.get("currentSerie").toString();
                    String nbrClicks =jsonObject.get("nbClic").toString();
                    String idCouloir = etIdCouloir.getText().toString();
                    intent.putExtra(MainActivity.ID_COULOIR,idCouloir  );
                    intent.putExtra(MainActivity.ID_SERIE, idSeries);
                    intent.putExtra(MainActivity.NBR_CLICKS, nbrClicks);
                    startActivity(intent);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });
    }

    @Override
    public void sendNegativeResponseToService(String response){
        try {
            JSONObject jsonObject = new JSONObject(response);
            String message = jsonObject.get("message").toString();
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    tvErrorMessage.setText(message);
                    tvErrorMessage.setVisibility(View.VISIBLE);
                }
            });
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
}
