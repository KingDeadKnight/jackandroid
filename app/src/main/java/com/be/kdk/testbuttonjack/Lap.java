package com.be.kdk.testbuttonjack;

import androidx.annotation.NonNull;

import java.io.Serializable;

public class Lap implements Serializable {

    private String time;

    public Lap(String time)
    {
        this.time = time;
    }

    public String getTime() {
        return time;
    }

    @NonNull
    @Override
    public String toString() {
        return this.getTime();
    }

}
