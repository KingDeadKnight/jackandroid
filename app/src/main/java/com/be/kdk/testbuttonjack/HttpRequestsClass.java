package com.be.kdk.testbuttonjack;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

public class HttpRequestsClass
{
    OkHttpClient client;
    public String url= "http://192.168.4.1:3000/serie/choosecouloir";
    //public String url= "http://192.168.1.10:3000/serie/choosecouloir";
    public String urlSendLaps= "http://192.168.4.1:3000/serie/newresultat";
    //public String urlSendLaps= "http://192.168.1.10:3000/serie/newresultat";
    public String urlAsksForStart= "http://192.168.4.1:3000/start";
    //public String urlAsksForStart= "http://192.168.1.10:3000/start";

    public HttpRequestsClass()
    {
        client = new OkHttpClient();
    }

    public void sendCouloirBooking(SendCouloirInterface inter, int couloir)
    {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("couloir", couloir);
            jsonObject.put("free",false);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        final MediaType JSON = MediaType.parse("application/json; charset=utf-8");

        RequestBody body = RequestBody.create(JSON, jsonObject.toString());
        Request request = new Request.Builder().post(body).url(url).build();

        client.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                call.cancel();
                JSONObject jsonObject = new JSONObject();
                try {
                    jsonObject.put("message", "Le serveur ne répond pas. Veuillez réessayer.");
                } catch (JSONException ex) {
                }
                inter.sendNegativeResponseToService(jsonObject.toString());
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {

                if(response.code()==200){
                    final String myResponse = response.body().string();
                    inter.sendPositiveResponseToService(myResponse);
                }
                else{
                    final String myResponse = response.body().string();
                    inter.sendNegativeResponseToService(myResponse);
                }
            }
        });
    }

    public void askToStart(StartAndStopTimerService inter)
    {
        Request request = new Request.Builder().url(urlAsksForStart).build();
        OkHttpClient customClient = new OkHttpClient.Builder()
                .connectTimeout(99, TimeUnit.MINUTES)
                .writeTimeout(99, TimeUnit.MINUTES)
                .readTimeout(99, TimeUnit.MINUTES)
                .build();
        customClient.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                call.cancel();
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {

                if(response.code()==200){
                    final String myResponse = response.body().string();
                    inter.startTimer(myResponse);
                }
            }
        });
    }

    public void sendAllLaps(StartAndStopTimerService inter, int idCouloirs, String idSerie, JSONArray laps, String lap) {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("couloir", idCouloirs);
            jsonObject.put("serieId",idSerie);
            jsonObject.put("tempsTotal",lap);
            jsonObject.put("lapList",laps);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        final MediaType JSON = MediaType.parse("application/json; charset=utf-8");

        RequestBody body = RequestBody.create(JSON, jsonObject.toString());
        Request request = new Request.Builder().post(body).url(urlSendLaps).build();
        client.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                call.cancel();
                sendAllLaps(inter,idCouloirs,idSerie,laps,lap);
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {

                if(response.code()==200){
                    final String myResponse = response.body().string();
                    inter.sendStopSignal(myResponse);
                }
            }
        });
    }
}
