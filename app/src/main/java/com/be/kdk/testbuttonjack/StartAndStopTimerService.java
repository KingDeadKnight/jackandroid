package com.be.kdk.testbuttonjack;

public interface StartAndStopTimerService
{
    void sendStopSignal(String myResponse);
    void startTimer(String myResponse);
}
