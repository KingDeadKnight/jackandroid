package com.be.kdk.testbuttonjack;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.Service;
import android.content.*;
import android.media.session.MediaSession;
import android.os.AsyncTask;
import android.os.Build;
import android.os.IBinder;
import android.widget.Toast;

import androidx.annotation.Nullable;

public class ChronoService extends Service implements StartAndStopTimerService
{
    Notification notif;
    Chronometer chronometer;
    MediaSession mSession;
    HttpRequestsClass httpRequestsClass;
    int idCouloirs;
    int nbreClicks;
    String idSerie;
    boolean isStarted;

    public static final String SERVICE_ID_COULOIR = "serviceIdCouloir";
    public static final String SERVICE_ID_SERIE = "serviceIdSerie";
    public static final String SERVICE_NBR_CLICKS= "serviceNbrClicks";
    public static final String INTENT_KEY_FOR_RESPONSE = "INTENT_KEY_FOR_RESPONSE";
    public static final String STRING_TO_SHOW = "StringToShow";
    public static final String STRING_TO_START = "StringToStart";
    String message = "Default message";


    @Override
    public void sendStopSignal(String myResponse) {
        Intent intent = new Intent(INTENT_KEY_FOR_RESPONSE);
        intent.putExtra(MainActivity.STOP_TIMER,"STOP");
        sendBroadcast(intent);
        chronometer.stop();
        isStarted = false;
    }

    @Override
    public void startTimer(String myResponse) {
        chronometer.start();
        isStarted = true;
        Intent intent = new Intent(INTENT_KEY_FOR_RESPONSE);
        intent.putExtra(MainActivity.START_TIMER,"START");
        sendBroadcast(intent);
        new ProgressAsyncTask().execute();
    }


    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        chronometer = new Chronometer(this);


        httpRequestsClass = new HttpRequestsClass();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        chronometer.stop();
    }

    public void updateNotif() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            notif = new Notification.Builder(this, "SERVICE_NOTIFICATION_CHANNEL")
                    .setContentTitle("ChronoService")
                    .setSmallIcon(R.drawable.ic_launcher_foreground)
                    .setContentText(chronometer.getCurrentTime())
                    .build();
        }
    }

    public void createNotificationChannel() {
        // Create the NotificationChannel, but only on API 26+ / Android 8+ because
        // the NotificationChannel class is new and not in the support library
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            CharSequence name = "Service Notification";
            String description = "Inform you if a service is running";
            int importance = NotificationManager.IMPORTANCE_NONE;
            NotificationChannel channel = new NotificationChannel("SERVICE_NOTIFICATION_CHANNEL", name, importance);
            channel.setDescription(description);
            NotificationManager notificationManager = getApplicationContext().getSystemService(NotificationManager.class);
            notificationManager.createNotificationChannel(channel);
        }
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        if (intent != null)
        {
            String idCouloirTempo = intent.getStringExtra(SERVICE_ID_COULOIR);
            String nbreClicksTempo = intent.getStringExtra(SERVICE_NBR_CLICKS);
            String idSerieTempo = intent.getStringExtra(SERVICE_ID_SERIE);

            intent.removeExtra(SERVICE_ID_COULOIR);
            intent.removeExtra(SERVICE_NBR_CLICKS);
            intent.removeExtra(SERVICE_ID_SERIE);
            if(idCouloirTempo!=null&&nbreClicksTempo!=null)
            {
                idCouloirs = Integer.parseInt(idCouloirTempo);
                nbreClicks = Integer.parseInt(nbreClicksTempo);
            }
            if(idSerieTempo!=null)
            {
                idSerie=idSerieTempo;
            }
            message = intent.getStringExtra(STRING_TO_START);
            if(message != null)
            {
                if (message.equals("StartChrono")){
                    httpRequestsClass.askToStart(this);
                }
                else if (message.equals("ChronoValue")){
                    if(nbreClicks>0)
                    {
                        chronometer.lap();
                        --nbreClicks;
                        if(nbreClicks==0)
                        {
                            httpRequestsClass.sendAllLaps(this,idCouloirs, idSerie, chronometer.getJsonLaps(), chronometer.getCurrentTime());
                        }
                    }
                }
                else if (message.equals("ResendTime"))
                {
                    Intent intentToSend = new Intent(INTENT_KEY_FOR_RESPONSE);
                    intent.putExtra("CurrentTimer",Long.toString(chronometer.getRawCurrentTime()));
                    sendBroadcast(intentToSend);
                }
            }
        }

        /* BOUTON JACK*/
        mSession = new MediaSession(getApplicationContext(), getPackageName());
        mSession.setActive(true);
        mSession.setCallback(new MediaSession.Callback() {
            @Override
            public boolean onMediaButtonEvent(Intent mediaButtonIntent) {
                Toast.makeText(ChronoService.this, "BONJOUR", Toast.LENGTH_SHORT).show();
                /*if(isStarted && nbreClicks>0)
                {//A tester !!!
                    chronometer.lap();
                    --nbreClicks;
                    if(nbreClicks==0)
                    {
                        httpRequestsClass.sendAllLaps(ChronoService.this,idCouloirs, idSerie, chronometer.getJsonLaps(), chronometer.getCurrentTime());
                    }
                }*/
                return super.onMediaButtonEvent(mediaButtonIntent);
            }
        });

        /*ANDROID 8+*/
        //TODO: VERIFIER SI LE SERVICE TOURNE SOUS DES VERSIONS PLUS ANCIENNES
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            createNotificationChannel();
            this.updateNotif();
            startForeground(1001, notif);
        }
        return START_STICKY; // Relauch service without intent
    }

    private class ProgressAsyncTask extends AsyncTask<Void, Void, Void> {

        @Override
        protected Void doInBackground(Void... voids) {
            while(true) {
                try {
                    publishProgress();
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }

        @Override
        protected void onProgressUpdate(Void... values) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                updateNotif();
                NotificationManager notificationManager = getApplicationContext().getSystemService(NotificationManager.class);
                notificationManager.notify(1001, notif);
            }
        }
    }
}