package com.be.kdk.testbuttonjack;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

public class LapAdapter extends RecyclerView.Adapter<LapAdapter.LapViewHolder> {

    private Chronometer chronometer;

    public LapAdapter (Chronometer chronometer)
    {
        this.chronometer = chronometer;
    }

    @NonNull
    @Override
    public LapViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        Context context = parent.getContext();
        LayoutInflater inflater = LayoutInflater.from(context);
        View view = inflater.inflate(R.layout.lap_item, parent, false);
        return new LapViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull LapViewHolder holder, int position) {
        Lap lap = chronometer.laps.get(position);
        TextView tvLap = holder.tvLap;
        tvLap.setText(lap.getTime());
    }

    @Override
    public int getItemCount() {
        return chronometer.laps.size();
    }

    public class LapViewHolder extends RecyclerView.ViewHolder
    {
        public TextView tvLap;

        public LapViewHolder(View itemView) {
            super(itemView);
            tvLap =itemView.findViewById(R.id.tvLap);
        }
    }
}
