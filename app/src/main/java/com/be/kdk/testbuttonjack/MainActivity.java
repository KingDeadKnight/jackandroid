package com.be.kdk.testbuttonjack;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;


public class MainActivity extends AppCompatActivity{

    private DataUpdateReceiver dataUpdateReceiver;


    public static final String ID_COULOIR = "idCouloir";
    public static final String ID_SERIE = "idSerie";
    public static final String NBR_CLICKS = "nbrClicks";
    public static final String STOP_TIMER = "stopTimer";
    public static final String START_TIMER = "startTimer";

    Button btnSendTime;
    Button btnGoToNextCourse;
    Chronometer chronometer;
    TextView tvInfoMessage;
    RecyclerView rvLap;

    LapAdapter adapter;

    int idCouloir;
    String idSeries;
    int nbreClicks;

    @Override
    protected void onSaveInstanceState(Bundle outState){
        super.onSaveInstanceState(outState);
        outState.putInt("idCouloir" , idCouloir);
        outState.putString("idSeries" , idSeries);
        outState.putInt("nbreClicks" , nbreClicks);
    }

    @Override
    protected  void onResume()
    {
        super.onResume();
        if (dataUpdateReceiver == null) dataUpdateReceiver = new DataUpdateReceiver();
        IntentFilter intentFilter = new IntentFilter(ChronoService.INTENT_KEY_FOR_RESPONSE);
        registerReceiver(dataUpdateReceiver, intentFilter);
    }

    @Override
    protected void onPause()
    {
        super.onPause();
        if (dataUpdateReceiver != null) unregisterReceiver(dataUpdateReceiver);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Intent intentCouloir = getIntent();
        if(intentCouloir != null)
        {
            idCouloir = Integer.parseInt(intentCouloir.getStringExtra(ID_COULOIR));
            idSeries = intentCouloir.getStringExtra(ID_SERIE);
            nbreClicks =Integer.parseInt( intentCouloir.getStringExtra(NBR_CLICKS));

        }
        btnSendTime = findViewById(R.id.btnSendTime);
        btnGoToNextCourse = findViewById(R.id.btnGoToNewCourse);
        tvInfoMessage = findViewById(R.id.tvInfoMessage);
        rvLap = findViewById(R.id.rvLaps);

        if(savedInstanceState !=null)
        {
            idCouloir = savedInstanceState.getInt("idCouloir");
            idSeries = savedInstanceState.getString("idSeries");
            nbreClicks = savedInstanceState.getInt("nbreClicks");
            Intent intent = new Intent(getApplicationContext(),ChronoService.class);
            intent.putExtra(ChronoService.STRING_TO_START, "ResendTime" );
            startService(intent);
        }
        else{
            Intent intent = new Intent(this, ChronoService.class);
            intent.putExtra(ChronoService.STRING_TO_SHOW, "Starting");
            intent.putExtra(ChronoService.SERVICE_ID_COULOIR,Integer.toString(idCouloir));
            intent.putExtra(ChronoService.SERVICE_NBR_CLICKS,Integer.toString(nbreClicks));
            intent.putExtra(ChronoService.SERVICE_ID_SERIE,idSeries);
            intent.putExtra(ChronoService.STRING_TO_START, "StartChrono" );
            startService(intent);
        }


        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        chronometer = findViewById(R.id.chrono);
        adapter = new LapAdapter(chronometer);
        rvLap.setAdapter(adapter);
        rvLap.setLayoutManager(new LinearLayoutManager(getApplicationContext()));
    }


    @Override
    protected void onDestroy()
    {
        super.onDestroy();
    }


    /*@Override
    public boolean onKeyDown(int keyCode, KeyEvent event)
    {
        if(keyCode == KeyEvent.KEYCODE_HEADSETHOOK)
        {
            Toast.makeText(getApplicationContext(),"FromMain",Toast.LENGTH_SHORT).show();
            chronometer.lap();
            Intent intent = new Intent(this, ChronoService.class);
            intent.putExtra(ChronoService.STRING_TO_START, "ChronoValue" );
            startService(intent);

        }
        return super.onKeyDown(keyCode, event);
    }*/


    public void AskServiceForLap(View view) {
        chronometer.lap();
        Intent intent = new Intent(this, ChronoService.class);
        intent.putExtra(ChronoService.STRING_TO_START, "ChronoValue" );
        startService(intent);
        adapter.notifyDataSetChanged();
    }

    public void goToNewCourse(View view) {//A tester !
        stopService(new Intent(this, ChronoService.class));
        finish();
    }

    private class DataUpdateReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent.getAction().equals(ChronoService.INTENT_KEY_FOR_RESPONSE)) {
                String message = intent.getStringExtra(STOP_TIMER);
                if(message!=null)
                {
                    if(message.equals("STOP"))
                    {
                        chronometer.stop();
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                tvInfoMessage.setText("La course est maintenant terminée.");
                                btnSendTime.setVisibility(View.INVISIBLE);
                                btnGoToNextCourse.setVisibility(View.VISIBLE);
                            }
                        });
                    }

                }
                String message3 = intent.getStringExtra("CurrentTimer");
                if (message3 != null)
                {
                    long currentTime = Long.parseLong(message3);
                    chronometer.setTimeElapsed(currentTime);
                }
                String message2 = intent.getStringExtra(START_TIMER);
                if (message2 != null)
                {
                    if(message2.equals("START"))
                    {
                        chronometer.start();
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                btnSendTime.setVisibility(View.VISIBLE);
                                tvInfoMessage.setText("La course a commencé !");
                            }
                        });
                    }
                }
            }
        }
    }
}